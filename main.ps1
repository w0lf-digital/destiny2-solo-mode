function deleteFirewallRules(){

}

foreach ($policy in Get-NetFirewallProfile){
    Write-Output "The $($policy.Name) firewall is $(if ($policy.Enabled) {"enabled."} else {"disabled ... enabling"})"

    if (!$policy.Enabled){
        Write-Output "(debug): enabling"
        try{
            Set-NetFirewallProfile -Profile $policy.Name -Enabled True
        }
        catch{
            Write-Output "ERROR['Caught Exception']: Failed to successfully enable the firewall!"
            Write-Output "Starting rollback procedure ..."
            # delete the rules
            exit
        }

        if (Get-NetFirewallProfile -Name $policy.Name | Select-Object -Property Enabled){
            Write-Output "Enabled successfully ..."
        }
        else{
            Write-Output "CAUTION: Failed to enable this firewall!"
        }
    }
    else{
        Write-Output "(debug): disabling"
        Set-NetFirewallProfile -Profile $policy.Name -Enabled False
    }

    #$refreshedRule = Get-NetFirewallProfile -Name $policy.Name
    #Write-Output "Refreshed rule status: $($refreshedRule.Enabled)"
    Write-Host "Testing $($policy.Name):"
    
    else{
        Write-Output "It's now disabled!"
    }
}